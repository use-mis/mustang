const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const morgan = require('morgan')
const {
  KAVENEGAR_API_KEY,
  PORT,
  API_TOKEN,
  OUR_NUMBERS,
  KAVENEGAR_SENDER: sender,
} = require('config')
const KavenegarApi = require('kavenegar').KavenegarApi({
  apikey: KAVENEGAR_API_KEY
})

const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db.json')
const db = low(adapter)

const googleMapUrl = (lat, lng) => `https://www.google.com/maps/dir/?api=1&destination=${lat},${lng}&travelmode=driving`

app.use(morgan('tiny'))
app.use(bodyParser.json())

app.set('view engine', 'ejs')


app.get('/admin', async (req, res, next) => {
  const requestsObject = db.get('requests')
  const last_id = db.get('last_id')
  const requests2 = [{a: 1}, {a: 2}]
  let requests = []
  for(request of requestsObject) {
    requests.push(request)
  }
  res.render('pages/admin', { requests, last_id })
})
app.post('/api/v1/repair-request', async (req, res, next) => {
  try {
    const {
      token
    } = req.headers
    if (token !== API_TOKEN) {
      return res.status(401).json({
        status: false,
        message: 'valid token is not provided!'
      })
    }

    const {
      customer_name,
      customer_cellphone,
      locality,
      customer_car,
      cusomter_car_number,
      latitude,
      longitude
    } = req.body

    const message = `دوست عزیز ${customer_name} سلام، ما درخواست شما مبتنی بر ارسال تعمیرکار به ناحیه ${locality} برای تعمیر خودروی ${customer_car} را دریافت کردیم. به سرعت یکی از تعمیرکاران ما با شما تماس خواهد گرفت`
    const response = await sendSms(message, customer_cellphone)


    const mapUrl = googleMapUrl(latitude, longitude)
    const serviceMessage = `مشتری جدید\nنام: ${customer_name}\nخودرو: ${customer_car}\nناحیه: ${locality}\nشماه همراه: ${customer_cellphone}\n${mapUrl}`
    const serviceResponse = await sendSms(serviceMessage, OUR_NUMBERS)

    const id = db.get('last_id').value() + 1
    db.get('requests')
      .push( { id, customer_name, customer_cellphone, 
        locality, customer_car, cusomter_car_number, 
        latitude, longitude, status: 'new', created_at: new Date(), updated_at: new Date() } )
      .write()
    db.update('last_id', n => n + 1)
      .write()

    res.json({
      status: true,
      message: 'booked!',
      order_id: id
    })

  } catch (e) {
    return next(e)
  }
});
app.use(express.static('picasso'))


// helpers
async function sendSms(message, receptor) {
  return new Promise((resolve, reject) => {
    KavenegarApi.Send({
        message,
        sender,
        receptor
      },
      (response, status) => {
        resolve(status)
      })
  })
}

app.listen(PORT, () => {
  console.log(`APP RUNING ON PORT ${PORT}`)
});